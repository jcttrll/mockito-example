package com.example;

public class SomeComponent {
	private Greeter greeter;

	public SomeComponent() {
		greeter = new Greeter();
	}

	public String render(String userName) {
		return "Content: " + greeter.greet(userName);
	}
}
