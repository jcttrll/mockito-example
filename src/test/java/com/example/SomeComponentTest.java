package com.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SomeComponentTest {
	@Mock
	private Greeter greeter;
	
	@InjectMocks
	private SomeComponent component;
	
	@Test
	public void renderIncludesGreetingFromGreeter() throws Exception {
		String expectedGreeting = "Hi!";
		when(greeter.greet(anyString())).thenReturn(expectedGreeting);

		assertThat(component.render(""), containsString(expectedGreeting));
	}
}
